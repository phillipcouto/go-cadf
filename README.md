This is the package to ease the implementation of CADF output for an application.

See [CADF | DMTF](https://www.dmtf.org/standards/cadf) for details.