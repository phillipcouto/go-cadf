package cadf

type Monitor struct {
	Initiator   interface{}
	Action      string //should only be monitor
	Target      interface{}
	Outcome     string
	Measurement interface{}
}

type Activity struct {
	Initiator   interface{}
	Action      string // see Action Taxonomy for list
	Target      interface{}
	Outcome     string
	Measurement interface{} //Optional
}

type Control struct {
	Initiator   interface{}
	Action      string //allow, deny, evaluate, notify
	Target      interface{}
	Outcome     string
	Measurement interface{} //Optional
	Reason      interface{}
}

type Outcome string

const (
	OSuccess Outcome = "success"
	OFailure Outcome = "failure"
	OUnkown  Outcome = "unknown"
	OPending Outcome = "pending"
)
