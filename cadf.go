package cadf

import (
	"time"
)

//See https://www.dmtf.org/sites/default/files/standards/documents/DSP0262_1.0.0.pdf
// and http://docs.openstack.org/developer/pycadf/event_concept.html
type Event map[string]interface{}

type EventType string

const (
	ETActivity EventType = "activity"
	ETControl  EventType = "control"
	ETMonitor  EventType = "monitor"
)

func NewEvent(et EventType) Event {
	e := make(Event)
	e["typeURI"] = "http://schemas.dmtf.org/cloud/audit/1.0/event"
	e["eventTime"] = time.Now().String()
	return e
}

func StartEvent(et EventType) Event {
	e := make(Event)
	e["typeURI"] = "http://schemas.dmtf.org/cloud/audit/1.0/event"
	e["beginTime"] = time.Now().String()
	return e
}

func (e Event) SetTags(t []string) Event {
	e["tags"] = t
	return e
}

func (e Event) GetTags() []string {
	if t, ok := e["tags"]; ok {
		return t.([]string)
	}
	return nil
}

func (e Event) FinishEvent() {
	if _, ok := e["beginTime"]; ok {
		e["endTime"] = time.Now().String()
	}
}
